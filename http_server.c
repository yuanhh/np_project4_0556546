#include<stdio.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netdb.h>
#include<netinet/in.h>
#include<string.h>
#include<unistd.h>
#include<stdlib.h>
#include<fcntl.h>
#include<sys/wait.h>
#include<ctype.h>
#include<arpa/inet.h>
#include"socket_conn.h"

#define SERVICE_PORT "8000"
#define MAX_HTTP_CLIENT 5
#define ACK_MSG "HTTP/1.1 200 OK"
#define CONTENT_TYPE "Content-Type: text/plain\n\n"
#define BUFFERSIZE 4096
#define CLIENT_STDOUT 1

struct request
{
    char *method;
    char *file_name;
    char *query_str;
    int content_len;
    struct in_addr addr;
};

void parse_request(char *, struct request*);
void set_env(struct request*);
int remote_batch_service(int conn_fd);
char *read_file(const char *file_name);

int main (int argc, char *argv[])
{
    int listen_fd = 0;
    int http_service_count = MAX_HTTP_CLIENT;

    listen_fd = passive_socket(SERVICE_PORT, "tcp", 0);

    while (1)
    {
        int conn_fd = wait_for_connection(listen_fd);

        int childpid = 0;
        if ((childpid = fork()) < 0)
        {
            fputs("ERROR on fork.\n", stderr);
        } 
        else if (childpid == 0)
        {
            close(listen_fd);
            dup2(conn_fd, 1);
            //dup2(conn_fd, 2);
            remote_batch_service(conn_fd);
            close(conn_fd);
            exit(0);
        } 
        else
        {
            close(conn_fd);
        }
    }
    close(listen_fd);
    wait(NULL);
    return 0;
}
char *read_file(const char *file_name) {

    char *web_content;
    long lSize = 0;
    FILE *fp = fopen(file_name, "rb");
    size_t result = 0;

    if (fp == NULL)
    {
        perror("ERROR on open .htm file\r\n");
        exit(1);
    }

    fseek(fp, 0, SEEK_END);
    lSize = ftell(fp);
    rewind(fp);

    web_content = (char *)malloc(sizeof(char)*lSize);
    if (web_content == NULL)
    {
        perror("ERROR on malloc.\r\n");
        exit(2);
    }

    result = fread(web_content, 1, BUFFERSIZE, fp);
    fclose(fp);
    return web_content;
}
void parse_request(char *msg, struct request *req)
{

    char *token = 0;
    char *saveptr = 0;
    int file_name_len = 0;
    
    token = strtok_r(msg, " ", &saveptr);
    req->method = strdup(token);

    token = strtok_r(saveptr, " ", &saveptr);
    file_name_len = strcspn(token, "? ") - 1;
    req->file_name = malloc(file_name_len + 1);
    memset(req->file_name, '\0', file_name_len + 1);
    strncpy(req->file_name, token + 1, file_name_len);

    if (*(token + 1 + file_name_len) == '?')
    {
        req->query_str = strdup(token + 1 + (file_name_len + 1));
    }
    else
    {
        req->query_str = 0;
    }

}
void set_env(struct request *req)
{
    setenv("REQUEST_METHOD", req->method, 1);
    setenv("SCRIPT_NAME" , req->file_name, 1);
    if (req->query_str == 0)
    {
        setenv("QUERY_STRING" , " ", 1);
    }
    else
    {
        setenv("QUERY_STRING" , req->query_str, 1);
    }
    setenv("CONTENT_LENGTH", " ", 1);
    setenv("REMOTE_HOST"   , " ", 1);
    setenv("REMOTE_ADDR"   , " ", 1);
    setenv("ANTH_TYPE"     , " ", 1);
    setenv("AUTH_TYPE"     , " ", 1);
    setenv("REMOTE_USER"   , " ", 1);
    setenv("REMOTE_IDENT"  , " ", 1);
}
int remote_batch_service(int conn_fd)
{
    char message_read[BUFFERSIZE] = {'\0'};
    int message_len = 0;
    char cwd[1024] = {'\0'};
    char file_name[1024] = {'\0'};
    struct request req;

    getcwd(cwd, sizeof(cwd));
    chroot(cwd);
    chdir(cwd);
    putenv("PATH=bin:.");

    memset(&req, 0, sizeof (struct request));

    message_len = read(conn_fd, message_read, BUFFERSIZE);
    write(CLIENT_STDOUT, ACK_MSG, strlen(ACK_MSG));
    write(CLIENT_STDOUT, CONTENT_TYPE, strlen(CONTENT_TYPE));

    parse_request(message_read, &req);

    set_env(&req);

    if ((strncmp(strchr(req.file_name, '.'), ".htm", 4)) == 0)
    {
        char *web_content = read_file(req.file_name);
        write(conn_fd, web_content, strlen(web_content));
    } 
    else if ((strncmp(strchr(req.file_name, '.'), ".cgi", 4)) == 0)
    {
        int childpid = 0;
        if ((childpid = fork()) <0)
        {
            perror("ERROR on fork.\n");
        } 
        else if (childpid == 0)
        {
            execlp(req.file_name, req.file_name, (char *)NULL);
            exit(-1);
        } 
    }
    else
    {
        perror("HTTP/1.1 404 Not Found");
    }

    return 0;
}
