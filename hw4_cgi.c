#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <unistd.h>
#include <sys/uio.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <errno.h>
#include "socket_conn.h"

#define MAX_CLIENT 5
#define BUFFERSIZE 30000
#define F_CONNECTING 0
#define F_READING 1
#define F_WRITING 2
#define F_DONE 3

struct connection_info
{
	char host_ip[16];
	char port[6];
	char proxy_ip[16];
	char proxy_port[6];
	char input_file_name[300];
	int conn_fd;
	FILE *input_fp;
	int status;
};
int readline(int fd,char *ptr,int maxlen, int *gSc);
int print_html(char *str, int col, int bold);
int parse_query(struct connection_info *conn_table);
void send_sock4_request(int conn_fd, struct connection_info conn_info);
char** strtok_bychar(char *cmds_in_line, const char delim, int *cmds_count);
void html_header();
void html_tail();

int main(int argc,char *argv[])
{
	int i = 0, x = 0;
	char msg_buf[BUFFERSIZE];
	struct connection_info conn_table[MAX_CLIENT];
	int gSc[MAX_CLIENT];
	int conn_count = 0;
	fd_set active_read_fds, read_fd_set, active_write_fds, write_fd_set;
	int nfds;

	memset(conn_table, 0, sizeof(struct connection_info) * 5);
	memset(gSc, 0, sizeof(int) * 4);
	FD_ZERO(&read_fd_set);
	FD_ZERO(&write_fd_set);
	FD_ZERO(&active_read_fds);
	FD_ZERO(&active_write_fds);

	html_header();

	parse_query(conn_table);

	printf("<table width=\"800\" border=\"1\">\r\n");
	printf("<tr>\n");

	for (i = 0; i < MAX_CLIENT; i++)
	{
		printf("<td>%s</td>\n", conn_table[i].host_ip);
		if (conn_table[i].host_ip != 0)
		{
			if (conn_table[i].proxy_ip != 0)
			{
				conn_table[i].conn_fd = connect_socket(conn_table[i].proxy_ip, conn_table[i].proxy_port, "tcp");
				set_nonblock(conn_table[i].conn_fd);
				send_sock4_request(conn_table[i].conn_fd, conn_table[i]);
			}
			else
			{
				conn_table[i].conn_fd = connect_socket(conn_table[i].host_ip, conn_table[i].port, "tcp");
				set_nonblock(conn_table[i].conn_fd);
			}
			FD_SET(conn_table[i].conn_fd, &active_read_fds);
			FD_SET(conn_table[i].conn_fd, &active_write_fds);
			char file_path[BUFFERSIZE] = {'\0'};
			snprintf(file_path, BUFFERSIZE, "testdata/%s", conn_table[i].input_file_name);
			conn_table[i].input_fp = fopen(file_path, "r");
			conn_table[i].status = F_CONNECTING;
			conn_count++;
		}
		else
		{
			conn_table[i].status = F_DONE;
		}
	}
	nfds = getdtablesize();

	read_fd_set = active_read_fds;
	write_fd_set = active_write_fds;

	printf("</tr>\r\n");
	printf("<tr>\r\n");

	for (i = 0; i < MAX_CLIENT; i++)
	{
		printf("<td valign=\"top\" id=\"m%d\"></td>\r\n", i);
	}

	printf("</tr>\r\n");
	printf("</table>");

	while (conn_count > 0)
	{
		memcpy(&read_fd_set, &active_read_fds, sizeof(read_fd_set));
		memcpy(&write_fd_set, &active_write_fds, sizeof(write_fd_set));

		x = select(nfds, &read_fd_set, &write_fd_set, NULL, NULL);
		if (x < 0 && errno != EINTR)
		{
			perror("select");
			for (i = 0; i < MAX_CLIENT; i++)
			{
				close(conn_table[i].conn_fd);
			}
			exit(EXIT_FAILURE);
		}

		for (i = 0; i < MAX_CLIENT; i++)
		{
			int len = 0;
			if (conn_table[i].status == F_DONE) continue;
			else if (conn_table[i].status == F_CONNECTING && (FD_ISSET(conn_table[i].conn_fd, &read_fd_set) || FD_ISSET(conn_table[i].conn_fd, &write_fd_set)))
			{
				int error;
				if (getsockopt(conn_table[i].conn_fd, SOL_SOCKET, SO_ERROR, &error, &len) < 0 || error != 0) {
					return (-1);
				}
				conn_table[i].status = F_READING;
				FD_CLR(conn_table[i].conn_fd, &active_write_fds);
			}
			else if (conn_table[i].status == F_READING && FD_ISSET(conn_table[i].conn_fd, &read_fd_set))
			{
				memset(msg_buf, '\0', BUFFERSIZE);
				len = readline(conn_table[i].conn_fd, msg_buf, BUFFERSIZE, &gSc[i]);
				if(len <= 0)
				{
					conn_table[i].status = F_DONE;
					FD_CLR(conn_table[i].conn_fd, &active_read_fds);
					close(conn_table[i].conn_fd);
					conn_table[i].conn_fd = 0;
					conn_count--;
					continue;
				}

				print_html(msg_buf, i, 0);

				if (gSc[i] == 1)
				{
					conn_table[i].status = F_WRITING;
					FD_CLR(conn_table[i].conn_fd, &active_read_fds);
					FD_SET(conn_table[i].conn_fd, &active_write_fds);
					gSc[i] = 0;
				}

			}
			else if (conn_table[i].status == F_WRITING && FD_ISSET(conn_table[i].conn_fd, &write_fd_set))
			{
				char cmd[BUFFERSIZE] = {'\0'};
				len = readline(fileno(conn_table[i].input_fp), cmd, BUFFERSIZE, &gSc[i]);

				print_html(cmd, i, 1);

				write(conn_table[i].conn_fd, cmd, strlen(cmd));

				conn_table[i].status = F_READING;
				FD_CLR(conn_table[i].conn_fd, &active_write_fds);
				FD_SET(conn_table[i].conn_fd, &active_read_fds);
			}
		}
	}

	for (i = 0; i < MAX_CLIENT; i++)
	{
		if (conn_table[i].conn_fd > 0)
			close(conn_table[i].conn_fd);
	}
	html_tail();

	return 0;
} 
int parse_query(struct connection_info *conn_table)
{
	char *query_str = getenv("QUERY_STRING");
	char **query_token = 0;
	int query_count = 0;
	int i = 0;

	query_token = strtok_bychar(query_str, '&', &query_count);

	for (i = 0; i < query_count; i++)
	{
		int entry = i / 5;
		switch (query_token[i][0])
		{
			case 'h':
				strcpy(conn_table[entry].host_ip, &query_token[i][3]);
				break;
			case 'p':
				strcpy(conn_table[entry].port, &query_token[i][3]);
				break;
			case 'f':
				strcpy(conn_table[entry].input_file_name, &query_token[i][3]);
				break;
			case 's':
				if (query_token[i][1] == 'h')
				{
					strcpy(conn_table[entry].proxy_ip, &query_token[i][4]);
				}
				else if (query_token[i][1] == 'p')
				{
					strcpy(conn_table[entry].proxy_port, &query_token[i][4]);
				}
				break;
			default:
				perror("query string format error.\n");
				return -1;
		}
	}
	return 0;
}
int print_html(char *str, int col, int bold)
{
	char *cptr = str;
	int i = 0;

	printf("<script>document.all['m%d'].innerHTML +=\"", col);

	if (bold)
		printf("<b>");
	for (i = 0; cptr[i] != '\0'; i++)
	{
		switch (cptr[i])
		{
			case '>':
				printf("&gt");
				break;
			case '<':
				printf("&lt");
				break;
			case '\r':
				break;
			case '\n':
				printf("<br>");
				break;
			case '"':
				printf("%s", "&quot;");
				break;
			default:
				printf("%c", cptr[i]);
				break;
		}
	}
	if(bold)
		printf("</b>");

	printf("\";</script>\n");

	fflush(stdout);
	return i;
}
int readline(int fd,char *ptr,int maxlen, int *gSc)
{
	int n,rc;
	char c;
	*ptr = 0;
	for(n=1;n<maxlen;n++)
	{
		if((rc=read(fd,&c,1)) == 1)
		{
			*ptr++ = c;
			if(c==' '&& *(ptr-2) =='%'){ *gSc = 1; break; }
			if(c=='\n')  break;
		}
		else if(rc==0)
		{
			if(n==1)     return(0);
			else         break;

		}
		else
		{
			if (errno == EAGAIN)
			{
				sleep(1);
				n--;
				continue;
			}
			return(-1);
		}

	}
	return(n);
} 
void send_sock4_request(int conn_fd, struct connection_info conn_info)
{
	byte buffer[8] = {0};
	unsigned int port = atoi(conn_info.port);
	int ip_feild_count = 0;
	char **ip = strtok_bychar(conn_info.host_ip, '.', &ip_feild_count);
	int i = 0;

	buffer[0] = 4;
	buffer[1] = 1;
	buffer[2] = port / 256;
	buffer[3] = port % 256;
	for (i = 0; i < ip_feild_count; i++)
	{
		buffer[i + 4] = atoi(ip[i]);
	}
	write(conn_fd, buffer, 8);
	read(conn_fd, buffer, 8);
}
char** strtok_bychar(char *cmds_in_line, const char delim, int *cmds_count)
{
	char **result = 0;
	char *cptr = cmds_in_line;
	char *token = 0;
	char *rest_of_cmd = NULL;
	int index = 0;
	int flag = 0;

	*cmds_count = 0;

	while (*cptr) {
		if ((*cptr != delim) && (flag == 0)) {
			(*cmds_count)++;
			flag = 1;
		} else if ((*cptr == delim) && (flag == 1)) {
			flag = 0;
		}
		cptr++;
	}
	while (result == NULL) {
		result = malloc(sizeof(char*) * ((*cmds_count) + 1));
	}
	token = strtok_r(cmds_in_line, &delim, &rest_of_cmd);

	for (index = 0; index < *cmds_count; index ++) {
		result[index] = strdup(token);
		token = strtok_r(rest_of_cmd, &delim, &rest_of_cmd); 
	}
	result[index] = NULL;

	return result;
}
void html_header()
{
	printf("%s",
			"<html>\r\n\
			<head>\r\n\
			<meta http-equiv=\"Content-Type\" content=\"text/html; charset=big5\" />\r\n\
			<title>Network Programming Homework 3</title>\r\n\
			</head>\r\n\
			<body bgcolor=#336699>\r\n\
			<font face=\"Courier New\" size=2 color=#FFFF99>\r\n"
		  );
	fflush(stdout);
}
void html_tail()
{
	printf("%s",
			"</font>\r\n\
			</body>\r\n\
			</html>\r\n"
		  );
	fflush(stdout);
}
