CC := gcc
CFLAGS := -g -Wall

all: http_server hw4.cgi socks_server

socks_server: socks_server.o socket_conn.o
	$(CC) $(CFLAG) socket_conn.o socks_server.o -o socks_server

socks_server.o: socks_server.c socket_conn.h
	$(CC) $(CFLAG) -c socks_server.c

hw4.cgi: hw4_cgi.o socket_conn.o
	$(CC) $(CFLAG) socket_conn.o hw4_cgi.o -o hw4.cgi 

hw4.o: hw4_cgi.c socket_conn.h
	$(CC) $(CFLAG) -c hw4_cgi.c

http_server: http_server.o socket_conn.o
	$(CC) $(CFLAG)  http_server.o socket_conn.o -o http_server

http_server.o: http_server.c socket_conn.h
	$(CC) $(CFLAG) -c http_server.c

socket_conn.o: socket_conn.c socket_conn.h
	$(CC) $(CFLAG) -c socket_conn.c

.PHONY : clean
clean:
	-rm -f *.o http_server socks_server hw4.cgi
