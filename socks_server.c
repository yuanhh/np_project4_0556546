#include<stdio.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netdb.h>
#include<netinet/in.h>
#include<string.h>
#include<unistd.h>
#include<stdlib.h>
#include<fcntl.h>
#include<sys/wait.h>
#include<ctype.h>
#include<arpa/inet.h>
#include<errno.h>
#include"socket_conn.h"

#define BUFFERSIZE 4096
#define SOCKS_PORT "9000"
#define BIND_PORT "9876"

struct request_info
{
	byte VN;
	byte CD;
	word32 DST_PORT;
	word32 DST_IP;
	char *USER_ID;
};

int sock4_service(int conn_fd);
int sock4_request(int conn_fd, struct request_info *req);
int connect_mode(int conn_fd, const struct request_info req);
int bind_mode(int conn_fd);
int data_bridge(int conn_fd, int dst_fd);

int main(int argc, int *argv[])
{
	int listen_fd = 0;

	listen_fd = passive_socket(SOCKS_PORT, "tcp", 0);

	while (1)
	{
		int childpid = 0;
		int conn_fd = wait_for_connection(listen_fd);

		if ((childpid = fork()) < 0)
		{
			perror("Fork:");
		}
		else if (childpid == 0)
		{
			close(listen_fd);
			sock4_service(conn_fd);
			exit(0);
		}
		else
		{
			close(conn_fd);
		}
	}

	close(listen_fd);
	return 0;
}

int sock4_service(int conn_fd)
{
	struct request_info req;
	int dst_fd = 0;

	memset(&req, 0, sizeof(struct request_info));

	sock4_request(conn_fd, &req);

	if (req.CD == 0x01)
	{
		dst_fd = connect_mode(conn_fd, req);

		if (dst_fd < 0)
		{
			printf("SOCKS_CONNECT FAILED ....\n");
			exit(-1);
		}
		printf("SOCKS_CONNECT GRANTED ....\n");
	}
	else if (req.CD == 0x02)
	{
		dst_fd = bind_mode(conn_fd);

		if (dst_fd < 0)
		{
			printf("BIND_LISTEN FAILED ....\n");
			exit(-1);
		}
		printf("BIND_LISTEN GRANTED ....\n");
	}

	data_bridge(conn_fd, dst_fd);

	return 0;
}
int sock4_request(int conn_fd, struct request_info *req)
{
	byte buffer[BUFFERSIZE] = {0};

	read(conn_fd, buffer, BUFFERSIZE);

	req->VN = buffer[0];
	req->CD = buffer[1];
	req->DST_PORT = buffer[2] << 8 | buffer[3];
	req->DST_IP = buffer[7] << 24 | buffer[6] << 16 | buffer[5] << 8 | buffer[4];
	req->USER_ID = buffer + 8;

	printf("------------request-----------\n");
	printf("Mode = %s USER_ID=%s\n",(req->CD == 0x02)? "BIND":"CONNECT", req->USER_ID);
	printf("Destination Addr = %u.%u.%u.%u\n", buffer[4], buffer[5], buffer[6], buffer[7]);
	printf("Destination Port = %u\n", req->DST_PORT);
	printf("------------------------------\n");

	return 0;
}
int connect_mode(int conn_fd, const struct request_info req)
{
	byte reply[8] = {0};
	int dst_fd = 0;
	char dst_ip[16] = {'\0'};
	char dst_port[10] = {'\0'};

	reply[0] = 0;
	reply[1] = 0x5A;
	reply[2] = req.DST_PORT / 256;
	reply[3] = req.DST_PORT % 256;
	reply[4] = (req.DST_IP >> 24) & 0xFF;
	reply[5] = (req.DST_IP >> 16) & 0xFF;
	reply[6] = (req.DST_IP >> 8) & 0xFF;
	reply[7] = req.DST_IP & 0xFF;

	snprintf(dst_ip, 16, "%u.%u.%u.%u", reply[7], reply[6], reply[5], reply[4]);
	snprintf(dst_port, 10, "%u", req.DST_PORT);

	dst_fd = connect_socket(dst_ip, dst_port, "tcp");
	if (dst_fd < 0)
	{
		perror("Connection:");
		return -1;
	}
	write(conn_fd, reply, 8);

	return dst_fd;
}
int bind_mode(int conn_fd)
{
	byte reply[8] = {0};
	int bind_fd = 0;
	struct sockaddr_in bind_addr;
	socklen_t bind_addr_len = sizeof(bind_addr);
	int dst_fd;

	memset(&bind_addr, 0, sizeof(bind_addr));

	bind_fd = passive_socket(BIND_PORT, "tcp", 0);

	if (getsockname(bind_fd, (struct sockaddr *)&bind_addr, &bind_addr_len) < 0)
	{
		perror("getsockname");
		exit(2);
	}

	unsigned int dst_port = ntohs(bind_addr.sin_port);

	reply[0] = 0;
	reply[1] = 0x5A;
	reply[2] = dst_port / 256;
	reply[3] = dst_port % 256;
	reply[4] = 0;
	reply[5] = 0;
	reply[6] = 0;
	reply[7] = 0;

	write(conn_fd, reply, 8);

	dst_fd = wait_for_connection(bind_fd);
	if (dst_fd  < 0)
	{
		exit(1);
	}
	write(conn_fd, reply, 8);

	return dst_fd;
}
int data_bridge(int conn_fd, int dst_fd)
{
	fd_set active_read_fds, read_fd_set;
	int nfds;
	char buffer[BUFFERSIZE] = {0};
	int x;

	FD_ZERO(&read_fd_set);
	FD_ZERO(&active_read_fds);

	FD_SET(conn_fd, &active_read_fds);
	FD_SET(dst_fd, &active_read_fds);

	nfds = getdtablesize();

	while (1)
	{
		int len = 0;
		memcpy(&read_fd_set, &active_read_fds, sizeof(read_fd_set));
		memset(buffer, 0, BUFFERSIZE);

		x = select(nfds + 1, &read_fd_set, NULL, NULL, NULL);
		if (x < 0 && errno != EINTR)
		{
			perror("select");
			close(conn_fd);
			close(dst_fd);
			exit(EXIT_FAILURE);
		}
		if (FD_ISSET(conn_fd, &read_fd_set))
		{
			len = read(conn_fd, buffer, sizeof(buffer));
			if (len <= 0)
			{
				close(conn_fd);
				close(dst_fd);
				break;
			}
			else
				write(dst_fd, buffer, len);
		}
		if (FD_ISSET(dst_fd, &read_fd_set))
		{
			len = read(dst_fd, buffer, sizeof(buffer));
			if (len <= 0)
			{
				close(conn_fd);
				close(dst_fd);
				break;
			}
			else
				write(conn_fd, buffer, len);
		}

	}
	return 0;
}
